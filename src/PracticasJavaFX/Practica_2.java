
package PracticasJavaFX;

import java.util.Random;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import static javafx.scene.text.FontWeight.BOLD;
import javafx.stage.Stage;

public class Practica_2 extends Application {
    Spinner spNumero1, spNumero2;
    Label etqNumero1, etqNumero2, etqNumero_Generado;
    Button btnGenerar;
    TextField tfNumero_Generado;
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
       primaryStage.setTitle("PRACTICA 2");//Creamos la escena
       //---------------------- PANE --------------------------
       Pane panel = new Pane();
       

       //---------------------- ETIQUETAS -------------------------
       etqNumero1 = new Label("Número 1");//Creamos etiqueta 1 con un titulo
       etqNumero1.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 14));//Hacemos que las letras de la etiqueta tenga una fuente personalizada, que este negritas y el tamaño
       etqNumero1.setLayoutX(20);//Ubicamos a la etiqueta X
       etqNumero1.setLayoutY(34);//Ubicamos a la etiqueta Y
       
       etqNumero2 = new Label("Número 2");//Creamos la etiqueta 2 con un titulo
       etqNumero2.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 14));//Hacemos que las letras de la etiqueta tenga una fuente personalizada, que este negritas y el tamaño
       etqNumero2.setLayoutX(20);//Ubicamos la etiqueta en X
       etqNumero2.setLayoutY(94);//Ubicamos la etiqueta en Y
       
       etqNumero_Generado = new Label("Número generado");//Creamos la etiqueta 2 con un titulo
       etqNumero_Generado.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 14));//Hacemos que las letras de la etiqueta tenga una fuente personalizada, que este negritas y el tamaño
       etqNumero_Generado.setLayoutX(20);//Ubicamos la etiqueta en X
       etqNumero_Generado.setLayoutY(154);//Ubicamos la etiqueta en Y
       
       
       // --------------------- SPINNER ---------------------------
       
       //Spinner 1
       spNumero1 = new Spinner();//Creamos un spinner 
       spNumero1.setPrefSize(100, 25);//Le damos un ancho y altura al spinner
       spNumero1.setLayoutX(160);//Lo ubicamos en X
       spNumero1.setLayoutY(30);//Lo ubicamos en Y
       SpinnerValueFactory<Integer> valor = new SpinnerValueFactory.IntegerSpinnerValueFactory(-100000, 100000, 0);//Creamos el rango del spinner1
       spNumero1.setValueFactory(valor);//Le asignamos el valor o el rango al spinner 1
       
       //Spinner 2
       spNumero2 = new Spinner();//Creamos un spinner 
       SpinnerValueFactory<Integer> valor2 = new SpinnerValueFactory.IntegerSpinnerValueFactory(-100000, 100000, 0);//Creamos el rango del spinner 2
       spNumero2.setPrefSize(100, 25);//Le damos un ancho y altura al spinner
       spNumero2.setLayoutX(160);//Lo ubicamos en X
       spNumero2.setLayoutY(90);//Lo ubicamos en Y
       spNumero2.setValueFactory(valor2);//Le asignamos el valor2 o el rango al spinner 2
       
       
       //---------------------- CAMPO DE TEXTO --------------------
       tfNumero_Generado = new TextField();//Creamos el campo de entrada
       tfNumero_Generado.setPrefSize(100, 25);//Le damos un ancho y una altura al campo de entrada
       tfNumero_Generado.setLayoutX(160);//Lo ubicamos en X
       tfNumero_Generado.setLayoutY(150);//Lo ubicamos en Y
       
       
       //-------------------------- BOTON --------------------------
       btnGenerar = new Button("Generar");
       btnGenerar.setLayoutX(180);
       btnGenerar.setLayoutY(200);
       
       btnGenerar.setOnAction(new EventHandler<ActionEvent>() {
           @Override
           public void handle(ActionEvent event) {
               //Nombre_Clase objeto = new Nombre_Clase();
                Random MiAleatorio = new Random(); //Creamos un objeto "mi aletorio" de tipo random
                int numeroA;
                int numeroB;
                int numeroAleatorio = 0;
                
                numeroA = (int) spNumero1.getValue();//Obtenemos el valor que esta en campo de texto del spinner 1 y lo convertimos a entero
                numeroB = (int) spNumero2.getValue();//Obtenemos el valor que esta en campo de texto del spinner 2 y lo convertimos a entero
                
                if (numeroA > numeroB) {
                    numeroAleatorio = (MiAleatorio.nextInt(numeroA-numeroB+1)+numeroB);//El numeroAleatorio tendra un valor con un rango del numeroB hasta el numeroA incluyendo ellos mismo
                }else{
                    if(numeroA < numeroB){
                        numeroAleatorio = (MiAleatorio.nextInt(numeroB-numeroA+1)+numeroA);//El numeroAleatorio tendra un valor con un rango del numeroA hasta el numeroB incluyendo ellos mismo
                    }else{
                        numeroAleatorio = numeroA | numeroB;//El numeroAleatorio tendra el valor del numeroA o NumeroB, ya que son iguales
                    }
                }
                //1° FORMA
                tfNumero_Generado.setText(numeroAleatorio+"");//Asignamos el valor de numeroAleatorio al campo de texto tfCampo
           }
       });
       
       panel.getChildren().addAll(etqNumero1,etqNumero2,etqNumero_Generado, spNumero1, spNumero2, tfNumero_Generado, btnGenerar);//Añadimos todos los componentes
       Scene scene = new Scene(panel,280,290);
       primaryStage.setScene(scene);
       primaryStage.show();
    }
}
