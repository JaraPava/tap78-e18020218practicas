
package PracticasJavaFX;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import static javafx.scene.text.FontWeight.BOLD;
import javafx.stage.Stage;

public class Practica_6 extends Application {
    Label lbOriginal, lbEspejo,lbOriginalE, lbEspejoE;
    RadioButton rbtnOpcio1,rbtnOpcion2, rbtnOpcion3, rbtnOpcio1E,rbtnOpcion2E, rbtnOpcion3E;
    CheckBox ckxOpcion4, ckxOpcion5, ckxOpcion6, ckxOpcion4E, ckxOpcion5E, ckxOpcion6E;
    TextField tfEntrada,tfEntradaE;
    ComboBox<String> cmbEntrada, cmbEntradaE;
    Spinner<Integer> spEntrada, spEntradaE;
    SelectionModel<String> model;
    SelectionModel<String> modelE; 
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("PRACTICA 6");//Le asignamos un titulo a la escena
        Pane panel = new Pane();//Creamos un panel
        
        
        //----------------------------- ETIQUETAS ------------------------------
        //ORIGINAL
        lbOriginal = new Label("ORIGINAL");//Creamos una etiqueta con un titulo
        lbOriginal.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 12));//Hacemos que el texto de la etiqueta este en negritas y con tamaño 12
        lbOriginal.setLayoutX(33);//Asignamos una ubicacion a la X
        lbOriginal.setLayoutY(30);//Asignamos una ubicacion a la Y
        
        //ESPEJO
        lbEspejo = new Label("ESPEJO");//Creamos una etiqueta con un titulo - ESPEJO
        lbEspejo.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 12));//Hacemos que el texto de la etiqueta este en negritas y con tamaño 12 - ESPEJO
        lbEspejo.setLayoutX(33);//Asignamos una ubicacion a la X
        lbEspejo.setLayoutY(183);//Asignamos una ubicacion a la Y
        
        
        //------------------------------ RADIO BUTTON -------------------------------
        //ORIGINAL
        rbtnOpcio1 = new RadioButton("Opcion 1");//Creamos un radioButton 1 opcion con un titulo Opcion 1
        rbtnOpcio1.setLayoutX(43);//Ubicamos la coordenada en X
        rbtnOpcio1.setLayoutY(64);//Ubicamos la coordenada en Y
        
        rbtnOpcion2 = new RadioButton("Opcion 2");//Creamos un radioButton 2 de opcion con un titulo Opcion 2
        rbtnOpcion2.setLayoutX(43);//Ubicamos la coordenanada en X
        rbtnOpcion2.setLayoutY(103);//Ubicamos la coordenada en Y
        
        rbtnOpcion3 = new RadioButton("Opcion 3");//Creamos un radioButton 3 de opcion con un titulo Opcion 3
        rbtnOpcion3.setLayoutX(43);//Ubicamos la coordenanada en X
        rbtnOpcion3.setLayoutY(140);//Ubicamos la coordenada en Y
        
        //ESPEJO
        rbtnOpcio1E = new RadioButton("Opcion 1");//Creamos un radioButton 1 opcion con un titulo Opcion 1 - ESPEJO
        rbtnOpcio1E.setLayoutX(43);//Ubicamos la coordenada en X
        rbtnOpcio1E.setLayoutY(214);//Ubicamos la coordenada en Y
        rbtnOpcio1E.setDisable(true);//Desactivamos el radioButton
        
        rbtnOpcion2E = new RadioButton("Opcion 2");//Creamos un radioButton 2 de opcion con un titulo Opcion 2 - ESPEJO
        rbtnOpcion2E.setLayoutX(43);//Ubicamos la coordenanada en X
        rbtnOpcion2E.setLayoutY(251);//Ubicamos la coordenada en Y
        rbtnOpcion2E.setDisable(true);//Desactivamos el radioButton
        
        rbtnOpcion3E = new RadioButton("Opcion 3");//Creamos un radioButton 3 de opcion con un titulo Opcion 3 - ESPEJO
        rbtnOpcion3E.setLayoutX(43);//Ubicamos la coordenanada en X
        rbtnOpcion3E.setLayoutY(286);//Ubicamos la coordenada en Y
        rbtnOpcion3E.setDisable(true);//Desactivamos el radioButton
        
        //------------------------------- CHECK BOX ---------------------------------
        //ORIGINAL
        ckxOpcion4 = new CheckBox("Opcion 4");//Creamos un checkBox 1 con un titulo 
        ckxOpcion4.setLayoutX(158);//Le asignamos una coordenada en X
        ckxOpcion4.setLayoutY(64);//Le asignamos una coordenanada en Y
        
        ckxOpcion5 = new CheckBox("Opcion 5");//Creamos un checkBox 2 con un titulo 
        ckxOpcion5.setLayoutX(158);//Le asignamos una coordenada en X
        ckxOpcion5.setLayoutY(103);//Le asignamos una coordenanada en Y
        
        ckxOpcion6 = new CheckBox("Opcion 6");//Creamos un checkBox 3 con un titulo 
        ckxOpcion6.setLayoutX(158);//Le asignamos una coordenada en X
        ckxOpcion6.setLayoutY(140);//Le asignamos una coordenanada en Y
        
        //ESPEJO
        ckxOpcion4E = new CheckBox("Opcion 4");//Creamos un checkBox 1 con un titulo - ESPEJO
        ckxOpcion4E.setLayoutX(158);//Le asignamos una coordenada en X
        ckxOpcion4E.setLayoutY(214);//Le asignamos una coordenanada en Y
        ckxOpcion4E.setDisable(true);//Desactivamos el checkBox
        
        ckxOpcion5E = new CheckBox("Opcion 5");//Creamos un checkBox 2 con un titulo - ESPEJO
        ckxOpcion5E.setLayoutX(158);//Le asignamos una coordenada en X
        ckxOpcion5E.setLayoutY(251);//Le asignamos una coordenanada en Y
        ckxOpcion5E.setDisable(true);//Desactivamos el checkBox
        
        ckxOpcion6E = new CheckBox("Opcion 6");//Creamos un checkBox 3 con un titulo - ESPEJO
        ckxOpcion6E.setLayoutX(158);//Le asignamos una coordenada en X
        ckxOpcion6E.setLayoutY(286);//Le asignamos una coordenanada en Y
        ckxOpcion6E.setDisable(true);//Desactivamos el checkBox
        
        //------------------------------ CAMPO DE TEXTO -------------------------------
        //ORIGINAL
        tfEntrada = new TextField();//Creamos un campo de entrada 
        tfEntrada.setLayoutX(262);//Le asignamos una coordenada en X
        tfEntrada.setLayoutY(60);//Le asignamos una coordenanda en Y
        tfEntrada.setPrefSize(110, 25);//Le asignamos el ancho y la altura del del campo de texto
        
        //ESPEJO
        tfEntradaE = new TextField();//Creamos un campo de entrada - ESPEJO
        tfEntradaE.setLayoutX(262);//Le asignamos una coordenada en X
        tfEntradaE.setLayoutY(210);//Le asignamos una coordenanda en Y
        tfEntradaE.setPrefSize(110, 25);//Le asignamos el ancho y la altura del del campo de texto
        tfEntradaE.setDisable(true);//Desactivamos el campo de texto
        
        
        //-------------------------------- COMBO BOX ----------------------------------
        ObservableList<String> lista = FXCollections.observableArrayList();//Creamos el una lista de elementos
        lista.add(0, "Item 1");//Primer Item
        lista.add(1, "Item 2");//Segundo Item
        lista.add(2, "Item 3");//Tercer Item
        lista.add(3, "Item 4");//Cuarto Item
        
        //ORIGINAL
        cmbEntrada = new ComboBox<>();//Creamos un combo box
        model = cmbEntrada.getSelectionModel();//Creamos un modelo para comboBox entrada y asi  poder seleccionar un item al iniciar el programa
        cmbEntrada.setLayoutX(262);//Asignamos una coordenada en X al comboBox
        cmbEntrada.setLayoutY(99);//Asignamos una coordenada en Y al comboBox
        cmbEntrada.setPrefSize(110, 25);//Le damos un ancho y una altura al comboBox
        cmbEntrada.setItems(lista);
        model.selectFirst();//Selecciona por predeterminado el primer item y lo muestra en el programa
        
        //ESPEJO
        cmbEntradaE = new ComboBox<>();//Creamos un combo box - ESPEJO
        modelE = cmbEntradaE.getSelectionModel();//Creamos un modelo para comboBox entrada y asi  poder seleccionar un item al iniciar el programa
        cmbEntradaE.setLayoutX(262);//Asignamos una coordenada en X al comboBox
        cmbEntradaE.setLayoutY(247);//Asignamos una coordenada en Y al comboBox
        cmbEntradaE.setPrefSize(110, 25);//Le damos un ancho y una altura al comboBox
        cmbEntradaE.setDisable(true);//Desactivamos el comboBox
        cmbEntradaE.setItems(lista);
        modelE.selectFirst();
        //--------------------------------- SPINNER -----------------------------------
        //ORIGINAL
        SpinnerValueFactory<Integer> valor = new SpinnerValueFactory.IntegerSpinnerValueFactory(-100000, 100000, 0);//Creamos el rango del spinner1
        spEntrada = new Spinner();//Creamos el spinner 
        spEntrada.setLayoutX(262);//Ubicamos al spinner con una coordenada en X
        spEntrada.setLayoutY(136);//Ubicamos al spinner con una coordenada en Y
        spEntrada.setPrefSize(110, 25);//Le asignamos un ancho y una altura al spinner
        spEntrada.setValueFactory(valor);//Le asignamos el rango de numeros del spinner
        spEntrada.setEditable(true);//Se puede editar el campo de texto del spinner
        
        //ESPEJO
        spEntradaE = new Spinner();//Creamos el spinner - ESPEJO
        spEntradaE.setLayoutX(262);//Ubicamos al spinner con una coordenada en X
        spEntradaE.setLayoutY(282);//Ubicamos al spinner con una coordenada en Y
        spEntradaE.setPrefSize(110, 25);//Le asignamos un ancho y una altura al spinner
        spEntradaE.setValueFactory(valor);//Le asignamos el rango de numeros del spinner
        spEntradaE.setDisable(true);//Desactivamos el spinner 
        
        
        //--------------------------------- EVENTOS -------------------------------------
        //EVENTO DEL RADIOBUTTON - OPCION 1
        rbtnOpcio1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(rbtnOpcio1.isSelected() == true){
                    rbtnOpcio1E.setSelected(true);
                }else{
                    rbtnOpcio1E.setSelected(false);
                }
            }
        });
        
        //EVENTO DEL RADIOBUTTON - OPCION 2
        rbtnOpcion2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (rbtnOpcion2.isSelected() == true) {
                    rbtnOpcion2E.setSelected(true);
                }else{
                    rbtnOpcion2E.setSelected(false);
                }
            }
        });
        
        //EVENTO DEL RADIOBUTTON OPCION 3
        rbtnOpcion3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (rbtnOpcion3.isSelected() == true) {
                    rbtnOpcion3E.setSelected(true);
                }else{
                    rbtnOpcion3E.setSelected(false);
                }
            }
        });
        
        //EVENTO DEL CHECKBOX - OPCION 4
        ckxOpcion4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (ckxOpcion4.isSelected() == true) {
                    ckxOpcion4E.setSelected(true);
                }else{
                    ckxOpcion4E.setSelected(false);
                }
            }
        });
        
        //EVENTO DEL CHECKBOX - OPCION 5
        ckxOpcion5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (ckxOpcion5.isSelected() == true) {
                    ckxOpcion5E.setSelected(true);
                }else{
                    ckxOpcion5E.setSelected(false);
                }
            }
        });
        
        //EVENTO DEL CHECKBOX - OPCION 6
        ckxOpcion6.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (ckxOpcion6.isSelected() == true) {
                    ckxOpcion6E.setSelected(true);
                }else{
                    ckxOpcion6E.setSelected(false);
                }
            }
        });
        
        //EVENTO DEL TEXTFIELD 
        tfEntrada.setOnKeyPressed(new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent event) {
                tfEntradaE.setText(tfEntrada.getText());
            }
        });
        
        //EVENTO DEL COMBOBOX 
        cmbEntrada.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int index = model.getSelectedIndex();
                modelE.select(index);
            }
        });
        panel.getChildren().addAll(lbOriginal, lbEspejo, rbtnOpcio1, rbtnOpcion2,
                                   rbtnOpcion3, ckxOpcion4, ckxOpcion5, ckxOpcion6,
                                   tfEntrada,cmbEntrada, spEntrada, rbtnOpcio1E,
                                   rbtnOpcion2E, rbtnOpcion3E, ckxOpcion4E, ckxOpcion5E, 
                                   ckxOpcion6E, tfEntradaE,cmbEntradaE, spEntradaE);
        
        Scene scene = new Scene(panel, 400, 360);//Creamos el escenario
        primaryStage.setScene(scene);//Le asignamos el escenario a la escena
        primaryStage.show();//Mostramos la escena
        
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
}
