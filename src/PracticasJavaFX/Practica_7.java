
package PracticasJavaFX;

import javafx.application.Application;
import javafx.stage.Stage;

public class Practica_7 extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("PRACTICA 7");//Le asignamos un titulo a la escena
    }
    
    public static void main(String[] args) {
        Application.launch(args);
    }
}
