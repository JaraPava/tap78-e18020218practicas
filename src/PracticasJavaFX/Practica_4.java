
package PracticasJavaFX;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import static javafx.scene.text.FontWeight.BOLD;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class Practica_4 extends Application{
    
    Label etqEscribe, etqPelicula;
    TextField tfEntrada_Pelicula;
    Button btnAnadir;
    ComboBox<String> cmbPelicula;
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        
        primaryStage.setTitle("PRACTICA 4");//A la escena se le asgina un titulo
        Pane panel = new Pane();//Creamos el panel
        
        //-------------------- ETIQUETAS -----------------------
        //ETIQUETA ESCRIBE
        etqEscribe = new Label("Escribe el titulo de una pelicula");//Creamos la etiqueta 1 con un titulo
        etqEscribe.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 12));//Al texto de la etiqueta le asignamos una fuente, lo hacemos en negritas y tendra un tamño 12
        etqEscribe.setLayoutX(41);//Le damos la coordenada en X
        etqEscribe.setLayoutY(34);//Le damos la coordenada en Y
        
        //ETIQUETA PELICULA
        etqPelicula = new Label("Peliculas");//Creamos la etiqueta 1 con un titulo
        etqPelicula.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 12));//Al texto de la etiqueta le asignamos una fuente, lo hacemos en negritas y tendra un tamño 12
        etqPelicula.setLayoutX(309);//Le damos la coordenada en X
        etqPelicula.setLayoutY(34);//Le damos la coordenada en Y
        
        
        //-------------------- CAMPO DE TEXTO ------------------
        //Entrada 1
        tfEntrada_Pelicula = new TextField();//Creamos el campo de texto 
        tfEntrada_Pelicula.setPrefSize(167, 25);//Le dimos un ancho y una altura al campo de texto
        tfEntrada_Pelicula.setLayoutX(41);//Le damos una coordenada en X
        tfEntrada_Pelicula.setLayoutY(75);//Le damos una coordenada en Y
        
        
        //----------------------- COMBO BOX --------------------
        //Combo Box 1
        cmbPelicula = new ComboBox<>();//Creamos un comboBox
        cmbPelicula.setPrefSize(167, 25);//Le asignamos una ancho y uan altura
        cmbPelicula.setLayoutX(248);//Le damos una coordenada en X
        cmbPelicula.setLayoutY(75);//Le damos una coordenada en Y
        
        
        //------------------------- BOTON ----------------------
        // Boton 1
        btnAnadir = new Button("Añadir");//Creamos un boton con un titulo
        btnAnadir.setLayoutX(99);//Le damos una coordenada en X
        btnAnadir.setLayoutY(123);//Le damos una coordenada en Y
        
        
        //------------------------- EVENTOS ---------------------
        btnAnadir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String pelicula = tfEntrada_Pelicula.getText();//Creamos una variable llamada pelicula para que almacene el nombre que ingresemos de la pelicula
                  boolean chismoso=true;
                  int x=0;
                  
                if (cmbPelicula.getItems().size()==0) {
                    cmbPelicula.getItems().add(pelicula);//Agrega la pelicula a la lista
                    cmbPelicula.setPromptText(pelicula);//Selecciona el elemento que se acaba de ingresar en la lista
                    tfEntrada_Pelicula.setText("");//borra lo que hay en el texto
                }else{
                    while (chismoso == true && x < cmbPelicula.getItems().size()) {//Creamos un ciclo para recorrer la lista y comparar con la siguiente condicion
                    if (pelicula.equalsIgnoreCase(cmbPelicula.getItems().get(x))) {//comparamos la pelicula que se ingreso en el campo de texto con el elemento del indice 
                            chismoso = false;
                        }else{
                            chismoso = true;
                        }
                        x++;
                    }
                    if (chismoso == true) {//Nos sirve para saber si hay numeros repetidos o no.
                        cmbPelicula.getItems().add(pelicula);//Agrega la pelicula a la lista
                        cmbPelicula.setPromptText(pelicula);//Selecciona el elemento que se acab de ingresar en la lista
                        tfEntrada_Pelicula.setText("");//borra lo que hay en el texto
                    }else{
                        JOptionPane.showMessageDialog(null, "Ya existe la pelicula");//Muestra una ventana diciendo que ya existe la pelicula
                        tfEntrada_Pelicula.setText("");//borra lo que hay en el texto
                    }  
                }     
            }
        });
        
        
        panel.getChildren().addAll(etqEscribe, etqPelicula, tfEntrada_Pelicula, cmbPelicula, btnAnadir);//Añade todos los nodos o componentes al panel
        Scene scene = new Scene(panel, 450, 200);//Creamos un escenario y le añadimos el panel con todos sus componentes y le damos un tamaño al escenario
        primaryStage.setScene(scene);//Le asignamos un escenario a la escena
        primaryStage.show();//Muestra la escena
        
        
    }
    
    public static void main(String[] args) {
        Application.launch(args);
    }
}
