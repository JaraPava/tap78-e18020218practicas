
package PracticasJavaFX;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Practica_5 extends Application{
    MenuBar mnubArchivo;
    Menu menu;
    MenuItem itmElementos;
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("PRACTICA 5");//Le asignamos un titulo a la escena
        Pane panel = new Pane();//Creamos el panel
        //------------------------------------ MENU ----------------------------------------
        menu = new Menu("File");//Creamos el menu
        
        
        //---------------------------------- MENU ITEMS -----------------------------------
        MenuItem mnuiAbrir = new MenuItem("Abrir");//Creamos un elemento llamado abrir para el MENU
        MenuItem mnuiSalir = new MenuItem("Salir");//Creamos un elemento llamado salir para el MENU
        
        
        //---------------------------------- MENU BAR --------------------------------------
        mnubArchivo = new MenuBar();//Creamos un MENU BAR
        mnubArchivo.setLayoutX(0);//Le asiganmos la ubicacion en X
        mnubArchivo.setLayoutY(0);//Le asignamos la ubicacion en Y
        
        menu.getItems().addAll(mnuiAbrir, mnuiSalir);//Añadimos los elementos al menu
        mnubArchivo.getMenus().add(menu);//Añadimos el menu al MENU BAR
        panel.getChildren().add(mnubArchivo);//Añadimos el MENU BAR al panel
        
        Scene scene = new Scene(panel, 300,200);//Le asignamos el panel al escenario con el tamaño del escenario
        primaryStage.setScene(scene);//Agregamos el escenario a la escena
        primaryStage.show();//Mostramos la escena
    }
}
