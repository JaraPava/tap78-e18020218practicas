
package PracticasJavaFX;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.CacheHint;
import javafx.scene.DepthTest;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import static javafx.scene.text.FontWeight.BOLD;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class Practica_1 extends Application{
    TextField tfCampo;
    Button btnSaludar;
    Label etqNombre;
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //------------------- Esto es la mesa ----------------------------
        primaryStage.setTitle("PRACTICA 1");//Nombre de la mesa
        
        //-------------------- Campo de texto ----------------------------
            tfCampo = new TextField();
            //jtfCampo.setPromptText("Hola");//Este metodo asigna un texto al campo de texto por default
            //jtfCampo.setAlignment(Pos.CENTER_LEFT);//Se posiciona en el centro
            //jtfCampo.setNodeOrientation(NodeOrientation.INHERIT);
            //jtfCampo.isVisible();
            //jtfCampo.setBlendMode(BlendMode.SRC_OVER);
            //jtfCampo.setCacheHint(CacheHint.DEFAULT);
            //jtfCampo.setDepthTest(DepthTest.INHERIT);
            //jtfCampo.setLayoutX(60);
            //jtfCampo.setLayoutY(80);
            
        //------------------------ Boton ---------------------------------
        btnSaludar = new Button();//Creamos el boton;
        btnSaludar.setText("Saludar");//Le asiganmos un titulo al boton
        
        //---------------------- Etiqueta --------------------------------
        etqNombre = new Label("Escriba su nombre: ");//Se creo una etiqueta con un titulo
        etqNombre.setFont(Font.font(STYLESHEET_MODENA, BOLD, 14));//Hacemos que la fuente sea negrita con un tamaño de 14 y con el estilo que marca

        //--------------------- Este es el mantel ------------------------
        //CONFIGURACION DEL MANTEL
        FlowPane flowPanel  =new FlowPane();//Creamos un flowPane
        flowPanel.setRowValignment(VPos.CENTER);//Establece la alineacion de la fila, en este caso esta en el centro
        flowPanel.setColumnHalignment(HPos.CENTER);//Establece la alineacion de la columna en este caso en el centro 
        flowPanel.setOrientation(Orientation.VERTICAL);//Hacemos que los elementos esten siempre verticales, nunca inverticales :V
        flowPanel.setAlignment(Pos.TOP_CENTER);//Hacemos que todos los nodos o componentes(etiqueta, campo de texto y el boton) se vayan al centro superior
        flowPanel.setVgap(20);//El tamaño de la brecha o espaciado de los elementos
        //AGREGAMOS LOS ELEMENTOS AL MANTEL
        flowPanel.getChildren().add(etqNombre);//Añade la etiqueta al mantel
        flowPanel.getChildren().add(tfCampo);//Añade el campo de texto al mantel
        flowPanel.getChildren().add(btnSaludar);//Añade el boton al mantel
        
        btnSaludar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String nombre = tfCampo.getText();//Guardamos lo que hay en el campo de entrada "tfCampo" a la variable nombre  
                if (nombre.isEmpty()==true) {
                    JOptionPane.showMessageDialog(null,"No ha ingresado un nombre");//Manda un mensaje emergente en donde advierte que no ja ingresado un nombre
                }
                JOptionPane.showMessageDialog(null,"Hola " + nombre);//Manda un mensaje emergente saludando 
            }
        });
        //Esta listo para usarse la mesa
        Scene escena = new Scene(flowPanel,300,200);//Creamos el lugar en donde se pondran las cosas, añadiendo el mantel y estableciendo el tamaño de la mesa
        primaryStage.setScene(escena);//La mesa esta lista para usarse
        primaryStage.show();//Muestra la mesa
    }
}
