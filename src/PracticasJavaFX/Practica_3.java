
package PracticasJavaFX;

import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import static javafx.scene.text.FontWeight.BOLD;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

public class Practica_3 extends Application{
    
    FileChooser fichero;
    TextField tfRuta;
    Button btnFichero;
    Label etqFichero;
    Pane panel;
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("PRACTICA 3");//Le asignamos un nombre a la escena
        
        panel = new Pane();//Creamos el panel
        
        //------------------------- ETIQUETA -------------------------
        etqFichero = new Label("Pulsa en el boton y elige una ruta");//Creamos una etiqueta y con un titulo
        etqFichero.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 13));//Le asignamos una fuente, en negritas con un tamaño 14
        etqFichero.setLayoutX(55);//Ubicamos la etiqueta en X
        etqFichero.setLayoutY(60);//Ubicamos la etiqueta en Y
        
        
        //----------------------- CAMPO DE TEXTO ---------------------
        tfRuta = new TextField();//Creamo un campo de texto
        tfRuta.setLayoutX(55);//Le asignamos una ubicacion en X
        tfRuta.setLayoutY(100);//Le asignamos una ubicacion en Y
        tfRuta.setPrefSize(330, 25);//Le asignamos un ancho y una altura
        
        
        //--------------------------- BOTON ----------------------------
        btnFichero = new Button("...");//Creamos un boton con un titulo
        btnFichero.setFont(Font.font(STYLESHEET_CASPIAN, BOLD, 13));//Hacemos que el texto del boton tenga una fuente, este en negritas y tenga un tamaño
        btnFichero.setLayoutX(395);//Ubicamos el boton en X
        btnFichero.setLayoutY(98);//Ubicamos el boton en Y
        
        //--------------------------- EVENTO ---------------------------
        
        btnFichero.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Creamos el objeto JFileChooser
                fichero = new FileChooser();
 
                //Abrimos la ventana, guardamos la opcion seleccionada por el usuario
                File archivo = fichero.showOpenDialog(null);
                // Creamos un archivo y le asignamos que archivo selecciono el ususuario
                if (archivo != null) {
                    tfRuta.setText(archivo.getAbsolutePath());
                }
                //Preguntamos si el archivo tiene informacion
                
            }
        });
        panel.getChildren().addAll(etqFichero, tfRuta, btnFichero);//Agregamos al panel todos los componentes
        Scene scene = new Scene(panel, 500, 250);//Creamos el escenario y le agregamos un panel y el tamaño 
        primaryStage.setScene(scene);//Agregamos el escenario a la escena
        primaryStage.show();//Muestra la escena
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
}
