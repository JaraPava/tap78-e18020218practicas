
package Practicas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Practica_4 {
    JLabel etiqueta1;
    JLabel etiqueta2;
    JTextField campo1;
    JButton btnGenerar;
    JComboBox despegable1;
    
    public static void main(String[] args) {
        Practica_4 p1 = new Practica_4();
        p1.run();
    }
    
    public void run(){
        
        JFrame ventana = new JFrame("PRACTICA 4");//Le asignamos el titulo a la ventana
        ventana.setSize(450, 200);//Establecemos el tamaño
        ventana.setLayout(null);
        ventana.setLocationRelativeTo(null);//Ubica en medio de la pantalla la ventana
        
        etiqueta1 = new JLabel("Escribe el titulo de una pelicula");//Asignamos el titulo de la etiqueta
        etiqueta1.setBounds(50, 40, 190, 30);//Ubicamos la etiqueta 1
        
        campo1 = new JTextField();//Creamo un JTextField llamado campo1
        campo1.setBounds(75, 70, 150, 25);//Ubicamos campo1 en la ventana
        
        btnGenerar = new JButton("Añadir");//Creamos un boton con el titulo añadir
        btnGenerar.setBounds(80, 105, 80, 30);//Ubicamos el boton en la ventana
        
        etiqueta2 = new JLabel("Peliculas");//Le asignamos el titulo a la etiequeta2
        etiqueta2.setBounds(285, 40, 100, 30);//Establecemos la ubicacion de la etiqueta2 a la ventana
        
        despegable1 = new JComboBox();//Creamos una lista despegable
        despegable1.setBounds(260, 70, 100, 25);//Establecemos la ubicacion 
        
        btnGenerar.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ae) {
                  String pelicula = campo1.getText();//Creamos una variable llamada pelicula para que almacene el nombre que ingresemos de la pelicula
                  boolean chismoso=true;
                  int x=0;
                  
                if (despegable1.getItemCount()==0) {
                    despegable1.addItem(pelicula);//Agrega la pelicula a la lista
                    despegable1.setSelectedItem(pelicula);//Selecciona el elemento que se acaba de ingresar en la lista
                    campo1.setText("");//borra lo que hay en el texto
                }else{
                    while (chismoso == true && x < despegable1.getItemCount()) {//Creamos un ciclo para recorrer la lista y comparar con la siguiente condicion
                    if (pelicula.equalsIgnoreCase((String) despegable1.getItemAt(x))) {//comparamos la pelicula que se ingreso en el campo de texto con el elemento del indice 
                            chismoso = false;
                        }else{
                            chismoso = true;
                        }
                        x++;
                    }
                    if (chismoso == true) {//Nos sirve para saber si hay numeros repetidos o no.
                        despegable1.addItem(pelicula);//Agrega la pelicula a la lista
                        despegable1.setSelectedItem(pelicula);//Selecciona el elemento que se acab de ingresar en la lista
                        campo1.setText("");//borra lo que hay en el texto
                    }else{
                        JOptionPane.showMessageDialog(null, "Ya existe la pelicula");//Muestra una ventana diciendo que ya existe la pelicula
                        campo1.setText("");//borra lo que hay en el texto
                    }  
                }         
            }
        });
        
        ventana.getContentPane().add(etiqueta1);//Agregamos la etiqueta a la ventana
        ventana.getContentPane().add(campo1);//Agregamos el campo a la ventana
        ventana.getContentPane().add(btnGenerar);//Agregamos el boton a la ventana
        ventana.getContentPane().add(etiqueta2);//Agregamos la etiqueta numero 2 a la ventana
        ventana.getContentPane().add(despegable1);//Agregamos la lista despegable a la ventana
        
        ventana.setVisible(true);//Hace visible la ventana
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//Cierra la ventana y termina el proceso.
    }
}
