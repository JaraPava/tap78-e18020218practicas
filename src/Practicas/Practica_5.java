
package Practicas;

import java.awt.MenuBar;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Practica_5 {
    JTextField campo1;
    public static void main(String[] args) {
        Practica_5 p1 = new Practica_5();
        p1.run();
    }
    
    public void run(){
        JFrame ventana = new JFrame("PRACTICA 5");//Creamos una ventana con el titulo de PRACTICA 5
        ventana.setSize(300, 200);//Le establecemos un tamaño
        ventana.setLocationRelativeTo(null);//LO centramos
        ventana.setLayout(null);//Le asignamos un layout o plantilla
        
        JMenuBar barra = new JMenuBar();//Creamos una barra
        
        JMenu File = new JMenu("File");//Creamos un menu
        JMenuItem abrir = new JMenuItem("abrir");//Creamos un elemento de menu llamado abrir
        JMenuItem salir = new JMenuItem("salir");//Creamos un elemento de menu llamado salir
        
        barra.setBounds(0, 0, 40, 20);//Establecemos la ubicacion de la barra de menu
        
        File.add(abrir);//Añadimos el elemento de menu "abrir" al menu "File"
        File.add(salir);//Añadimos el elemento de menu "salir" al menu "File"
        barra.add(File);//Añadimos el menu "File" al menu de barra "barra"
        
        campo1 = new JTextField();
        campo1.setBounds(50, 50, 200, 30);
        
        ventana.getContentPane().add(barra);//Añadimos el menu de barra a la ventana
        ventana.getContentPane().add(campo1);
        ventana.setVisible(true);//Muestra la ventana
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//Cierra la ventana y detiene la ejecucion
    }
}
