
package Practicas;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
public class Practica_3 {
    JFrame ventana;
    JFileChooser fichero1;
    JTextField campo1;
    JLabel etiqueta1;
    JButton boton1;
    private JPanel contentPane;
    private JTextField textField;
    private JTextArea textArea;
    String resultado;
        
    public static void main(String[] args) {
        Practica_3 app = new Practica_3();
        app.run();
        
    }
    
    void run(){
        ventana = new JFrame("MOSTRAR RUTA FICHERO");
        ventana.setSize(500, 250);
        ventana.setLayout(null);
        ventana.setLocationRelativeTo(null);
        
        etiqueta1 = new JLabel("Pulsa en el boton y elige una ruta");
        etiqueta1.setBounds(60, 60, 320, 20);
        
        campo1 = new JTextField();
        campo1.setBounds(60, 100, 320, 20);
        
        boton1 = new JButton("...");
        
        boton1.setBounds(400, 100, 30, 20);
        
        boton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                //Creamos el objeto JFileChooser
                JFileChooser fc=new JFileChooser();
 
                //Abrimos la ventana, guardamos la opcion seleccionada por el usuario
                fc.showOpenDialog(contentPane);
                // Creamos un archivo y le asignamos que archivo selecciono el ususuario
                File archivo = fc.getSelectedFile();
                //Preguntamos si el archivo tiene informacion
                if (archivo != null) {
                    campo1.setText(archivo.getAbsolutePath());//Si el archivo es diferente de nulo al campo1 se le asigna la direccion del archivo
                }
            }
            
        });
        
        
        ventana.getContentPane().add(etiqueta1);
        ventana.getContentPane().add(campo1);
        ventana.getContentPane().add(boton1);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
    public void obtenerRuta(String resultado){
        this.campo1.setText(resultado);
    }
}
