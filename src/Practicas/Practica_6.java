
package Practicas;

import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Practica_6 {
   JRadioButton radio1;
   JRadioButton radio2;
   JRadioButton radio3;
   JCheckBox casilla1;
   JCheckBox casilla2;
   JCheckBox casilla3;
   JTextField campo1;
   JComboBox caja1;
   JSpinner spin1;
   JLabel original;
   
   JRadioButton radio1E;
   JRadioButton radio2E;
   JRadioButton radio3E;
   JCheckBox casilla1E;
   JCheckBox casilla2E;
   JCheckBox casilla3E;
   JTextField campo1E;
   JComboBox caja1E;
   JSpinner spin1E;
   JLabel espejo;
   
    public static void main(String[] args) {
        Practica_6 app = new Practica_6();
        app.run();
    }
   public void run(){
       
       JFrame ventana = new JFrame("PRACTICA 6");//Creamos la ventana con el titulo PRACTICA 6
       ventana.setSize(400, 360);//Establecemos el tamaño de la ventana
       ventana.setLayout(null);//Pegamos la plantilla a la ventana
       ventana.setLocationRelativeTo(null);//Hacemos que la ventana se ubique en el centro
       SpinnerNumberModel modeloS = new SpinnerNumberModel();//Por si las dudas
        
       //----------- ESTA ES LA PARTE ORIGINAL -----------//
       original = new JLabel("ORIGINAL");//Creamos una etiqueta con el titulo ORIGINAL
       original.setBounds(30, 20, 80, 20);//Establecemos la ubicacion de la etiqueta
       
       radio1 = new JRadioButton("Opcion 1");//Creamos un Boton circular con la leyenda Opcion 1
       radio1.setBounds(50, 60, 100, 20);//Establecemos la ubicacion del boton circular
       
       radio2 = new JRadioButton("Opcion 2");//Creamos un Boton circular con la leyenda Opcion 2
       radio2.setBounds(50, 90, 100, 20);//Establecemos la ubicacion del boton circular
       
       radio3 = new JRadioButton("Opcion 3");//Creamos un Boton circular con la leyenda Opcion 3
       radio3.setBounds(50, 120, 100, 20);//Establecemos la ubicacion del boton circular
       
       casilla1 = new JCheckBox("Opcion 4");//Creamos un Boton de opcion cuadrangular con la leyenda Opcion 4
       casilla1.setBounds(160, 60, 100, 20); //Establecemos la ubicacion del boton de opcion cuadrangular
       
       casilla2 = new JCheckBox("Opcion 5");//Creamos un Boton de opcion cuadrangular con la leyenda Opcion 5
       casilla2.setBounds(160, 90, 100, 20); //Establecemos la ubicacion del boton de opcion cuadrangular
       
       casilla3 = new JCheckBox("Opcion 6");//Creamos un Boton de opcion cuadrangular con la leyenda Opcion 6
       casilla3.setBounds(160, 120, 100, 20);//Establecemos la ubicacion del boton de opcion cuadrangular
       
       campo1 = new JTextField();//Creamos un campo de texto
       campo1.setBounds(260, 60, 80, 20);//Establecemos la ubicacion del campo de texto
       
       caja1 = new JComboBox();//Creamos una caja con lista despegable 
       caja1.setBounds(260, 90, 80, 20);//Establecemos la ubicacion de caja
       
       spin1 = new JSpinner();//Creamos un spinner
       spin1.setBounds(260, 120, 80, 20);//Establecemos la ubicacion del spinner
       
       //----------- ESTA ES LA PARTE ESPEJO-----------//
       espejo = new JLabel("ESPEJO");//Creamos una etiqueta con el titulo ESPEJO
       espejo.setBounds(30, 160, 80, 20);//Establecemos la ubicacion de la etiqueta
       
       radio1E = new JRadioButton("Opcion 1");//Creamos un Boton circular con la leyenda Opcion 1 Espejo
       radio1E.setBounds(50, 200, 100, 20);//Establecemos la ubicacion del boton circular Espejo
       radio1E.setEnabled(false);//Hacemos que el boton circular este desabilitado
       
       radio2E = new JRadioButton("Opcion 2");//Creamos un Boton circular con la leyenda Opcion 2 Espejo
       radio2E.setBounds(50, 230, 100, 20);//Establecemos la ubicacion del boton circular Espejo
       radio2E.setEnabled(false);//Hacemos que el boton circular este desabilitado
       
       radio3E = new JRadioButton("Opcion 3");//Creamos un Boton circular con la leyenda Opcion 3 Espejo
       radio3E.setBounds(50, 260, 100, 20);//Establecemos la ubicacion del boton circular Espejo
       radio3E.setEnabled(false);//Hacemos que el boton circular este desabilitado
       
       casilla1E = new JCheckBox("Opcion 4");//Creamos un Boton de opcion cuadrangular con la leyenda Opcion 4 Espejo
       casilla1E.setBounds(160, 200, 100, 20); //Establecemos la ubicacion del boton de opcion cuadrangular Espejo
       casilla1E.setEnabled(false);//Hacemos que el boton cuadrangular este desabilitado
       
       casilla2E = new JCheckBox("Opcion 5");//Creamos un Boton de opcion cuadrangular con la leyenda Opcion 5 Espejo
       casilla2E.setBounds(160, 230, 100, 20);//Establecemos la ubicacion del boton de opcion cuadrangular  Espejo
       casilla2E.setEnabled(false);//Hacemos que el boton cuadrangular este desabilitado
       
       casilla3E = new JCheckBox("Opcion 6");//Creamos un Boton de opcion cuadrangular con la leyenda Opcion 6 Espejo
       casilla3E.setBounds(160, 260, 100, 20);//Establecemos la ubicacion del boton de opcion cuadrangular Espejo
       casilla3E.setEnabled(false);//Hacemos que el boton cuadrangular este desabilitado
       
       campo1E = new JTextField();//Creamos un campo de texto
       campo1E.setBounds(260, 200, 80, 20);//Establecemos la ubicacion del campo de texto
       campo1E.setEnabled(false);//Hacemos que el campo de texto este desabilitado
       
       caja1E = new JComboBox();//Creamos una caja de lista despegable Espejo
       caja1E.setBounds(260, 230, 80, 20);//Damos una ubicacion a dicha caja
       caja1E.setEnabled(false);//Hacemos que la caja este desabilitada
       
       spin1E = new JSpinner();//Creamos el spinner
       spin1E.setBounds(260, 260, 80, 20);//Ubicamos el spinner
       spin1E.setEnabled(false);//Hacemos que el spinner se desabilite
       
       radio1.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               //Clonamos los movimientos de la parte original a la parte espejo
               if (radio1.isSelected()) {
                   radio1E.setSelected(true);
               }else{
                   radio1E.setSelected(false);
               }
           }
       });
       
       radio2.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               //Clonamos los movimientos de la parte original a la parte espejo
               if (radio2.isSelected()) {
                   radio2E.setSelected(true);
               }else{
                   radio2E.setSelected(false);
               }
           }
       });
       radio3.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               //Clonamos los movimientos de la parte original a la parte espejo
               if (radio3.isSelected()) {
                   radio3E.setSelected(true);
               }else{
                   radio3E.setSelected(false);
               }
           }
       });
       
       casilla1.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent ae) {
               //Clonamos los movimientos de la parte original a la parte espejo
               if (casilla1.isSelected()) {
                   casilla1E.setSelected(true);
               }else{
                   casilla1E.setSelected(false);
               }
           }
       });
       
       casilla2.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent ae) {
               //Clonamos los movimientos de la parte original a la parte espejo
               if (casilla2.isSelected()) {
                   casilla2E.setSelected(true);
               }else{
                   casilla2E.setSelected(false);
               }
           }
       });
       
       casilla3.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent ae) {
               //Clonamos los movimientos de la parte original a la parte espejo
               if (casilla3.isSelected()) {
                   casilla3E.setSelected(true);
               }else{
                   casilla3E.setSelected(false);
               }
           }
       });
       
       campo1.addKeyListener(new KeyListener() {
           @Override
           public void keyTyped(KeyEvent ke) {
               
           }

           @Override
           public void keyPressed(KeyEvent ke) {
               //Clonamos los movimientos de la parte original a la parte espejo
               if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                   String dato = campo1.getText();
                   caja1.addItem(dato);
                   caja1.setSelectedItem(dato);
                   caja1E.addItem(dato);
                   caja1E.setSelectedItem(dato);
                   campo1E.setText(dato);
               }
           }

           @Override
           public void keyReleased(KeyEvent ke) {
               
           }
       });
       
       caja1.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               //Clonamos los movimientos de la parte original a la parte espejo
               Object elemento = caja1.getSelectedItem();
               caja1E.setSelectedItem(elemento);
           }
       });
       
       spin1.addChangeListener(new ChangeListener() {
           @Override
           public void stateChanged(ChangeEvent ce) {
               int numero = (int) spin1.getValue();
               spin1E.setValue(numero);
           }
       });
       
       ventana.getContentPane().add(original);//Añadimos la etiqueta a la ventana
       ventana.getContentPane().add(radio1);//Añadimos el boton circular de opcion en la ventana
       ventana.getContentPane().add(radio2);//Añadimos el boton circular de opcion en la ventana
       ventana.getContentPane().add(radio3);//Añadimos el boton circular de opcion en la ventana
       ventana.getContentPane().add(casilla1);//Añadimos el boton de casilla en la ventana
       ventana.getContentPane().add(casilla2);//Añadimos el boton de casilla en la ventana
       ventana.getContentPane().add(casilla3);//Añadimos el boton de casilla en la ventana
       ventana.getContentPane().add(campo1);//Añadimos el campo a la ventana
       ventana.getContentPane().add(caja1);//Añadimos la caja de lista despegable
       ventana.getContentPane().add(spin1);//Añadimos spinner a la ventana
       ventana.getContentPane().add(espejo);//Añadimos la etiqueta espejo
       ventana.getContentPane().add(radio1E);//Añadimos el boton circular de opcion en la ventana
       ventana.getContentPane().add(radio2E);//Añadimos el boton circular de opcion en la ventana
       ventana.getContentPane().add(radio3E);//Añadimos el boton circular de opcion en la ventana
       ventana.getContentPane().add(casilla1E);//Añadimos el boton de casilla en la ventana
       ventana.getContentPane().add(casilla2E);//Añadimos el boton de casilla en la ventana
       ventana.getContentPane().add(casilla3E);//Añadimos el boton de casilla en la ventana
       ventana.getContentPane().add(campo1E);//Añadimos el campo a la ventana
       ventana.getContentPane().add(caja1E);//Añadimos la caja de lista despegable
       ventana.getContentPane().add(spin1E);//Añadimos spinner a la ventana
       
       ventana.setVisible(true);//Hacemos visible la ventana
       ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//Cuando cierre la ventana este metodo se encarga de detener la ejecucion
       
   }
}
